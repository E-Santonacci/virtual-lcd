# Affichage sur écran LCD
programme, Node JS, permettant de simuler l’affichage de chiffres sur un écran LCD sur la console.

## Description
Cet écran dispose d’une grille composée d’éléments de taille 3x3. Chaque case peut contenir un espace, un tiret bas, ou un pipe.

Pour chaque chiffre, voici l’élément de taille 3x3 attendu (ici en utilisant un point à la place d’espace)

 
    ._.   ...   ._.   ._.   ...   ._.   ._.   ._.   ._.   ._.
    |.|   ..|   ._|   ._|   |_|   |_.   |_.   ..|   |_|   |_|
    |_|   ..|   |_.   ._|   ..|   ._|   |_|   ..|   |_|   ..|

Donc, la grille correspondante au nombre 910 serait :

    ._. ... ._.
    |_| ..| |.|
    ..| ..| |_|

## Prérequis
Cette application est un script Node JS sur la ligne de commande, vous devez avoir Node JS ( > v10) installé et opérationnel sur votre machine. Vous pouvez trouver Node JS pour les différents OS ici:

[https://nodejs.org/en/](https://nodejs.org/en/)


## Installation
Cloner le repository Git ou bien le télécharger.

``git clone https://gitlab.com/E-Santonacci/virtual-lcd.git``

installer les modules complémentaires

``cd virtual-lcd``  
``npm install``

## lancer le programme
Vous pouvez lancer l'application avec le nombre en paramètre:

    > node app.js 310
    _         _   
    _|    |  | |
    _|    |  |_|

Vous pouvez également lancer l'application sans paramètre, un nombre vous sera demandé:

    > node app.js
    Entrer un nombre de maximum 10 chiffres (tapez q pour quitter): 9574
     _    _    _        
    |_|  |_     |  |_|
      |   _|    |    |

