/**
 * @file app.js
 *
 * @copyright (c) 2020 Abhaya
 * @author Eric Santonacci <eric.santonacci@abhaya.fr>
 *
 * @description Simuler l’affichage de chiffres sur un écran LCD
 *
 */

const readlineSync = require('readline-sync');

// On vérifie s'il y a une valeur sur la ligne de commande
let value = '';
let argument = '';
if (process.argv.length > 2) {
    argument = process.argv[2];
}

while (!value) {
    if (argument) {
        value = argument;
        argument = '';
    } else {
        value = readlineSync.question('Entrer un nombre de maximum 10 chiffres (tapez q pour quitter): ');
    }

    if (value === 'q') {
        process.exit(0);
    }

    // on test si longueur comprise entre 1 et 10 et composé uniquement de chiffre
    if (!/^\d{1,10}$/.test(value)) {
        console.log("  /!\\ : La chaine est trop longue ou n'est pas composée uniquement de chiffres.\n")
        value = '';
    }
}

const separator = '  '; // séparateur de chiffres
const digits = value.split('');
// 3 lignes
for (let line = 0; line < 3; line++) {
    // pour chaque chiffre
    for (let i = 0; i < digits.length; i++) {
        switch (line) {
            case 0:
                switch (digits[i]) {
                    case '0':
                    case '2':
                    case '3':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        process.stdout.write(' _ ');
                        break;
                    case '1':
                    case '4':
                        process.stdout.write('   ');
                        break;

                }
                process.stdout.write(separator);
                break;
            case 1:
                switch (digits[i]) {
                    case '0':
                        process.stdout.write('| |');
                        break;
                    case '1':
                    case '7':
                        process.stdout.write('  |');
                        break;
                    case '2':
                    case '3':
                        process.stdout.write(' _|');
                        break;
                    case '4':
                    case '8':
                    case '9':
                        process.stdout.write('|_|');
                        break;
                    case '5':
                    case '6':
                        process.stdout.write('|_ ');
                        break;
                }
                process.stdout.write(separator);
                break;
            case 2:
                switch (digits[i]) {
                    case '0':
                    case '6':
                    case '8':
                        process.stdout.write('|_|');
                        break;
                    case '1':
                    case '4':
                    case '7':
                    case '9':
                        process.stdout.write('  |');
                        break;
                    case '2':
                        process.stdout.write('|_ ');
                        break;
                    case '3':
                    case '5':
                        process.stdout.write(' _|');
                        break;
                }
                process.stdout.write(separator);
                break;
        }
    }
    // nouvelle ligne
    console.log('');
}
